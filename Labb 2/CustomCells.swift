//
//  TableViewCell.swift
//  Labb 2
//
//  Created by Alven Enwya on 2018-12-21.
//  Copyright © 2018 Alven Enwya. All rights reserved.
//

import UIKit

class Section {
    init(title: String, cells: [DataCell]) {
        self.title = title
        self.cells = cells
    }
    var title: String
    var cells: [DataCell]
}

var sections = [Section]()

class DataCell {
    init(image: UIImage, title: String, date: String, desc: String) {
        self.image = image
        self.title = title
        self.date = date
        self.desc = desc
    }
    
    var image: UIImage
    var title: String
    var date: String
    var desc: String
    
}

class HeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bg.backgroundColor = UIColor.lightGray
    }

    func initValues(headerTitle: String){
        self.title.text = headerTitle
    }
    

}

class BodyCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func initValues(title: String, date: String, image: UIImage){
        self.cellTitle.text = title
        self.cellDate.text = date
        self.cellImage.image = image
    }
    
}
