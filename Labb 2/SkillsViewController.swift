//
//  SkillsViewController.swift
//  Labb 2
//
//  Created by Alven Enwya on 2018-12-21.
//  Copyright © 2018 Alven Enwya. All rights reserved.
//

import Foundation
import UIKit

class SkillsViewController: UIViewController {
    
    
    @IBOutlet weak var myImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Skills"
        
        UIView.animate(withDuration: 4) {
            self.myImage.center.x += self.view.bounds.width
        }
    }
}
