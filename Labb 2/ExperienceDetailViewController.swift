//
//  ExperienceDetailViewController.swift
//  Labb 2
//
//  Created by Alven Enwya on 2018-12-21.
//  Copyright © 2018 Alven Enwya. All rights reserved.
//

import Foundation
import UIKit

class ExperienceDetailViewController: UIViewController {
    
    
    @IBOutlet weak var myImage: UIImageView!
    var _myImage: UIImage!
    @IBOutlet weak var myTitle: UILabel!
    var _myTitle: String!
    @IBOutlet weak var myDate: UILabel!
    var _myDate: String!
    @IBOutlet weak var myBody: UILabel!
    var _myBody: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = _myTitle
        
        self.myImage.image = _myImage
        self.myTitle.text = _myTitle
        self.myDate.text = _myDate
        self.myBody.text = _myBody
        self.myBody.sizeToFit()
    }
}
