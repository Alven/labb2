//
//  TableViewController.swift
//  Labb 2
//
//  Created by Alven Enwya on 2018-12-21.
//  Copyright © 2018 Alven Enwya. All rights reserved.
//

import Foundation
import UIKit

class ExperienceViewController: UITableViewController {
    
    var myImage: UIImage!
    var myTitle: String!
    var myDate: String!
    var myDesc: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (sections.count == 0) {
            // mock data
            sections.append(Section(
                title: "Work Experience",
                cells: [
                    DataCell(image: UIImage(named: "cctv.jpeg")!, title: "CCTV", date: "2010-2015", desc: "I have worked with installing security cameras in different place in sweden."),
                    DataCell(image: UIImage(named: "truck.jpeg")!, title: "Truck", date: "2008-2009", desc: "I have been worked as a truck driver for a while.")
                ]))
            
            sections.append(Section(
                title: "Education",
                cells: [
                    DataCell(image: UIImage(named: "cnc2.jpeg")!, title: "CNC", date: "2013-2015", desc: "I have studied CNC operator for one year in a school in Eskilstuna and i got my first and  funes job ther . We had used coding to make thos machins work we used som thing called G-code and there i started to be interested in programming.")
                ]))
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        let count = sections.count
        return count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = sections[section].cells.count
        return count
    }
    
    // handle header cells
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! HeaderCell
        
        headerCell.initValues(headerTitle: sections[section].title)
        
        return headerCell
    }
    // handle data cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! BodyCell
        
        let section = indexPath.section
        let row = indexPath.row
        cell.initValues(title: sections[section].cells[row].title, date: sections[section].cells[row].date, image: sections[section].cells[row].image)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sec = indexPath.section
        let row = indexPath.row
        
        let cell = sections[sec].cells[row]
        
        self.myImage = cell.image
        self.myTitle = cell.title
        self.myDate = cell.date
        self.myDesc = cell.desc
        
        
        performSegue(withIdentifier: "toDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            guard let dest = segue.destination as? ExperienceDetailViewController else { return }
            
            dest._myTitle = self.myTitle
            dest._myDate = self.myDate
            dest._myBody = self.myDesc
            dest._myImage = self.myImage
        }
        
    }
}
